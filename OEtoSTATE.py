import numpy as np
#mu is given in km^3/s^2

#This function converts the (6) orbital elements into the cartesian dynamical state in the same inertial reference system where the elements are given (for example the mean ecpliptic and equinox of date ref system).
#In the case of hyperbolic and elliptic orbits "a" is the semimajor axis, while in parabolic orbits "a" is replaced by "p" (since in that case "e=1" and "a" is infinite). Otherwise for parabolic motion, if "q" is given then "p=2q" must be passed to the OEtoState function.
#a is given in AU
#i,W,pi,L are given in deg
def oetostate(a,e,i,W,pi,L,mu):
	M=L-pi			   #mean anomaly in deg
	w=pi-W	           #pericenter argument in deg
	M=M/180.*np.pi     #conversion of mean anomaly in rad
	w=w/180.*np.pi     #conversion of pericenter argument in rad
	i=i/180.*np.pi     #inclination in rad
	W=W/180.*np.pi     #ascending node longitude in rad
	a=a*149597870.7    #conversion of semimajor axis in km
	if(e==1.):   #the orbit is parabolic and M=tan(f/2)+tan^3(f/2)/3
		f=2.*np.arctan((3.*M/2.+np.sqrt((3.*M/2.)**2.+1.))**(1./3.)+(3.*M/2.-np.sqrt((3.*M/2.)**2.+1.))**(1./3.))       #true anomaly in rad
		p=a     #the first orbital element given is replaced by parameter p for parabolic orbits (in km)
	if(e<1.):    #the orbit is elliptical and M=E-e*sin(E)
		#Newton-Raphson:
		tol=1e-6
		funz=lambda E:e*np.sin(E)-E+M
		der=lambda E:e*np.cos(E)-1.
		E=0.
		Eold=10.
		while(abs(E-Eold)>tol):
			Eold=E
			E=E-funz(E)/der(E)
		f=2.*np.arctan(np.sqrt((1.+e)/(1.-e))*np.tan(E/2.))       #true anomaly in rad
		p=a*(1.-e**2.)            #in km
	if(e>1.):    #the orbit is hyperbolic and M=e*sinh(F)-F
		#Newton-Raphson:
		tol=1e-6
		funz=lambda F:e*np.sinh(F)-F-M
		der=lambda F:e*np.cosh(F)-1.
		F=0.
		Fold=10.
		while(abs(F-Fold)>tol):
			Fold=F
			F=F-funz(F)/der(F)
		f=2.*np.arctan(np.sqrt((e+1.)/(e-1.))*np.tanh(F/2.))      #true anomaly in rad
		p=a*(1.-e**2.)            #in km
	r=p/(1.+e*np.cos(f))	#in km
	h=np.sqrt(mu*p)         #in km^2/s
	#position coordinates and velocity components in the Laplace ref system:
	X=r*np.cos(f)           #in km
	Y=r*np.sin(f)           #in km
	Z=0.                    #in km
	Xdot=-h/p*np.sin(f)     #in km/s
	Ydot=h/p*(e+np.cos(f))  #in km/s
	Zdot=0.                 #in km/s
	#position vector in Laplace system (in km):
	Rlaplace=np.array([X,Y,Z])
	#velocity vector in Laplace system (in km/s):
	Vlaplace=np.array([Xdot,Ydot,Zdot])
	#Rotation matrix from inertial to Laplace:
	R=np.array([[np.cos(W)*np.cos(w)-np.sin(W)*np.sin(w)*np.cos(i),np.sin(W)*np.cos(w)+np.cos(W)*np.sin(w)*np.cos(i),np.sin(w)*np.sin(i)],
		        [-np.cos(W)*np.sin(w)-np.sin(W)*np.cos(w)*np.cos(i),-np.sin(W)*np.sin(w)+np.cos(W)*np.cos(w)*np.cos(i),np.cos(w)*np.sin(i)],
		        [np.sin(W)*np.sin(i),-np.cos(W)*np.sin(i),np.cos(i)]])
	#Rotation matrix from Laplace to inertial:
	RT=np.transpose(R)
	#position vector in inertial system (in km):
	Rinertial=np.dot(RT,Rlaplace)
	#velocity vector in inertial system (in km/s):
	Vinertial=np.dot(RT,Vlaplace)
	return np.array([Rinertial[0],Rinertial[1],Rinertial[2],Vinertial[0],Vinertial[1],Vinertial[2]])