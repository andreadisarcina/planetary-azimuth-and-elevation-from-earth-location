# Planetary Azimuth and Elevation from Earth location

The script contains a function to compute the azimuth A and elevation h for a specified Earth location. It is applied to compute an (A,h)—ephemeris of Mercury, Venus and Mars for each day in 2021 at 21:00 for Padua.

The txt files are the outputs containing the computed (A,h)-ephemeris.

The file EvsA.pdf is a plot of the apparent motion of the Planets on the plane of the sky.

The script uses the function OEtoSTATE to convert the orbit elements into the Cartesian dynamical state.
